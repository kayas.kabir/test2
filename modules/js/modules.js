var navIconContainer = document.getElementById('nav-icon-container');
var openNav = document.getElementById('open_nav');
var closeNav = document.getElementById('close_nav');
var navbar = document.getElementById('navbar');

openNav.addEventListener('click', function() {
    navbar.classList.add('navbarOpened');
    openNav.classList.add('fa-bars-after');
    closeNav.classList.add('fa-times-after');
});

closeNav.addEventListener('click', function() {
    navbar.classList.remove('navbarOpened');
    openNav.classList.remove('fa-bars-after');
    closeNav.classList.remove('fa-times-after');
});

var slideBtn = document.getElementById('slideBtn');
var cardsContainer1 = document.getElementById('module1');
var cardsContainer2 = document.getElementById('module2');
var cardsContainer3 = document.getElementById('module3');
var title1 = document.getElementById('title1');
var title2 = document.getElementById('title2');
var title3 = document.getElementById('title3');

slideBtn.addEventListener('click', ()=>{
    cardsContainer1.classList.add('cardAnimLeft');
    cardsContainer2.classList.add('cardAnimLeft');
    cardsContainer3.classList.add('cardAnimLeft');
    title1.classList.add('cardAnimLeft');
    title2.classList.add('cardAnimLeft');
    title3.classList.add('cardAnimLeft');
});



// addClassBtn.addEventListener('click', ()=>{
// });
