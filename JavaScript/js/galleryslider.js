const slide = document.querySelector('.slide-container');
const item = document.querySelectorAll('.slide-container img');

//buttons
const prevBtn = document.querySelector('#prev');
const nextBtn = document.querySelector('#next');

//counter
var counter = 1;
const size = item[0].clientWidth; // all images in the list must be of same size as that of 1st image

slide.style.transform = 'translateX(' + (-size * counter) + 'px)';  // !important

//slide changing functions
function nextSlide(){
	if (counter >= item.length - 1) return;
	slide.style.transition = 'transform 0.4s ease-in-out';
	counter++;

	slide.style.transform = 'translateX(' + (-size * counter) + 'px)';
}

function prevSlide(){
	if (counter <= 0) return;
	slide.style.transition = 'transform 0.4s ease-in-out';
	counter--;
	
	slide.style.transform = 'translateX(' + (-size * counter) + 'px)';
}

//animate
var timer = null;
function setTimer(){
	timer=setInterval(function(){
		nextSlide();
	}, 3000);
}

//setTimer();

function pauseSlide(){
	clearInterval(timer);
	timer=null;
	animateBtn.innerHTML = "animate";
}

//looping items function
slide.addEventListener('transitionend', () => {

	if(item[counter].id === 'lastClone'){
		slide.style.transition = 'none';
		counter = item.length - 2;

		slide.style.transform = 'translateX(' + (-size * counter) + 'px)';
	}

	if(item[counter].id === 'firstClone'){
		slide.style.transition = 'none';
		counter = item.length - counter;

		slide.style.transform = 'translateX(' + (-size * counter) + 'px)';
	}
});

//btn event calling
nextBtn.addEventListener('click', pauseSlide);
nextBtn.addEventListener('click', nextSlide);
prevBtn.addEventListener('click', pauseSlide);
prevBtn.addEventListener('click', prevSlide);