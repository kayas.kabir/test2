function _(id){
    return document.getElementById(id);
}

function audioApp() {
    var audio = new Audio ();
    var audio_folder = 'media/';
    var audio_ext = ".mp3";
    var audio_index = 1;
    var is_playing = false;
    var playingtrack;
    var trackbox = _('trackbox');
    var audio_bar = _('audio_bar');
    var tracks = {
            'track1':["Havana", "havana"],
            'track2':['Nischal', 'nischal'],
            'track3':["No Tears Left To Cry", 'no_tears_left_to_cry'],
            'track4':['Never Be The Same', 'never_be_the_same'],
            'track5':['Shristi Ra Drishti', 'shristi_ra_drishti']
        };
    
    for(var track in tracks){
        var tb = document.createElement('div');
        var pb = document.createElement('button');
        var tn = document.createElement('div');
        tb.className = 'trackbar';
        pb.className = 'playbtn';
        tn.className = 'trackname';
        tn.innerHTML = audio_index+". "+tracks[track] [0];
        pb.id = tracks[track] [1];
        pb.addEventListener('click', switchTrack);
        tb.appendChild(pb);
        tb.appendChild(tn);
        trackbox.appendChild(tb);
        audio_index++;
    }

    audio.addEventListener('ended', function() {
        _(playingtrack).style.background = "url(D:/HTML/JavaScript/img/playbtn.png)";
        _(playingtrack).style.backgroundSize = "cover";
        playingtrack = '';
    });

    function switchTrack(event){
        if(is_playing) {
            if (playingtrack != event.target.id) {
                is_playing = true;
                _(playingtrack).style.background = "url(D:/HTML/JavaScript/img/playbtn.png)";
                _(playingtrack).style.backgroundSize = "cover";
                event.target.style.background  = "url(D:/HTML/JavaScript/img/pausebtn.png)";
                event.target.style.backgroundSize = "cover";
                audio.src = audio_folder + event.target.id + audio_ext;
                audio.play();
            } else {
                audio.pause();
                is_playing = false;
                event.target.style.background = "url(D:/HTML/JavaScript/img/playbtn.png)";
                event.target.style.backgroundSize = "cover";
            }
        } else{
            is_playing = true;
            event.target.style.background = "url(D:/HTML/JavaScript/img/pausebtn.png)";
            event.target.style.backgroundSize = "cover";
            if(playingtrack != event.target.id){
                audio.src = audio_folder + event.target.id + audio_ext;
            }
            audio.play();
        }
        playingtrack = event.target.id;
    }
}

window.addEventListener('load', audioApp);