$(document).ready(function(){

    $('#themeToggler').click(function(){
        $('.body-light').toggleClass('body-dark');
        $('.themeToggler-light').toggleClass('themeToggler-dark');
        $('.fa-moon').toggleClass('fa-sun');
    });

});