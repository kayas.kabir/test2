var time = 0;
var running = 0;

function startPausel(){
	if(running == 0){
		running = 1;
		leftIncrement();
		document.getElementById('startPause').innerHTML = "Pause";		
	}

	else{
		running = 0;
		document.getElementById('startPause').innerHTML = "Resume";		
		//record();
	}
}
/*
function startPauser(){
	if(getElementById('left_timer') = 1){
		running = 1;
		rightIncrement();
		document.getElementById('startPause').innerHTML = "Pause";		
	}

	//else{
		//running = 0;
		//document.getElementById('startPause').innerHTML = "Resume";		
	//}
}*/

function leftIncrement(){
	if(running == 1){
		setTimeout(function(){
			time++;
			var mins = Math.floor(time/10/60);
			var secs = Math.floor(time/10);
			var tenths = time % 10;
			

			if(mins < 10){
				mins = "0" + mins;
			}

			while(secs > 59){
				secs = secs - 60;
			}

			if(secs < 10){
				secs = "0" + secs;
			}

			document.getElementById("left_timer").innerHTML = mins + " " + ":" + " " + secs + " " + ":" + " " + "0" + tenths;
			//document.getElementById("right_timer").innerHTML = mins + " " + ":" + " " + secs + " " + ":" + " " + "0" + tenths;
			leftIncrement();
		}, 100);
	} 
}

function rightIncrement(){
	if(running == 1){
		setTimeout(function(){
			time++;
			var mins = Math.floor(time/10/60);
			var secs = Math.floor(time/10);
			var tenths = time % 10;

			if(mins < 10){
				mins = "0" + mins;
			}

			while(secs > 59){
				secs = secs - 60;
			}

			if(secs < 10){
				secs = "0" + secs;
			}

			//document.getElementById("left_timer").innerHTML = mins + " " + ":" + " " + secs + " " + ":" + " " + "0" + tenths;
			document.getElementById("right_timer").innerHTML = mins + " " + ":" + " " + secs + " " + ":" + " " + "0" + tenths;
			rightIncrement();
		}, 100);
	} 
}

var leftTimer = document.getElementById("left_timer").innerHTML;

function record(){
	
	document.getElementById("lap_lists").innerHTML = leftTimer;
	
}