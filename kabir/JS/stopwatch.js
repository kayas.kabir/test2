var time = 0;
var running = 0;

function startPause(){
	if(running == 0){
		running = 1;
		increment();
		document.getElementById('startPause').innerHTML = "Pause";		
	}
	else{
		running = 0;
		document.getElementById('startPause').innerHTML = "Resume";		
	}

	alternate();
}

function reset(){
	running = 0;
	time = 0;
	document.getElementById('startPause').innerHTML = "Start";		
	document.getElementById('timer_left').innerHTML = "00 : 00 : 00";
	document.getElementById('timer_right').innerHTML = "00 : 00 : 00";
}

function increment(){
	if(running == 1){
		setTimeout(function(){
			time++;
			var mins = Math.floor(time/10/60);
			var secs = Math.floor(time/10);
			var tenths = time % 10;
			var round = 

			if(mins < 10){
				mins = "0" + mins;
			}

			if(secs >= 60){
				secs = "0" + tenths;
			}

			if(secs < 10){
				secs = "0" + secs;
			}

			document.getElementById("timer_left").innerHTML = mins + " " + ":" + " " + secs + " " + ":" + " " + "0" + tenths;
			//document.getElementById("timer_right").innerHTML = mins + " " + ":" + " " + secs + " " + ":" + " " + "0" + tenths;
			increment();
		}, 100);
	}

}

function alternate(){
	if(running == 1){	
	getElementById('timer_left').innerHTML = increment();
	getElementById('timer_right').innerHTML = "00 : 00 : 00";
	}

}

setTimeout(function(){ },1000)